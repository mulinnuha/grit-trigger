# Neo4j Listener

## Prerequisites

- Java 8
- Maven (optional)

## Supported transaction

- [x] Create node
- [x] Create edge
- [x] Add/update node properties
- [x] Add/update edge properties
- [ ] Delete node
- [ ] Delete edge
- [ ] Delete node properties
- [ ] Delete edge properties
- [ ] Get transaction executor

## Known limitation

- Current version supports one label only per node (a node can't have multiple label)

## Installation

### Alternative 1: Copy compiled jar file

1. Copy `jar/triggers-1.0.jar` to the `plugins/` directory of Neo4j server.

2. Restart Neo4j server

### Alternative 2: Build jar plugin (requires Maven)

1. Clone: `git clone https://gitlab.com/mulinnuha/grit-trigger.git`

2. Build: `mvn clean package`

3. Copy `target/triggers-1.0.jar` (or `jar/triggers-1.0.jar`) to the `plugins/` directory of Neo4j server.

4. Restart Neo4j Server