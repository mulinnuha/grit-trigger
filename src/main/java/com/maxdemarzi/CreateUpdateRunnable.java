package com.maxdemarzi;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.event.PropertyEntry;
import org.neo4j.graphdb.event.TransactionData;

import javax.xml.bind.SchemaOutputResolver;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringJoiner;

public class CreateUpdateRunnable implements Runnable {

    private static TransactionData td;
    private static GraphDatabaseService db;
    private static final String FILENAME = System.getProperty("user.home") + "/.grit/log.txt";

    public CreateUpdateRunnable (TransactionData transactionData, GraphDatabaseService graphDatabaseService) {
        td = transactionData;
        db = graphDatabaseService;

    }

    @Override
    public void run() {
        try (Transaction tx = db.beginTx()) {
            String username = td.username();

            StringBuilder contents = new StringBuilder();
            //Created Node
            for (Node node : td.createdNodes()) {
                HashMap<String, String> content = new HashMap();
                String id = String.valueOf(node.getId());
                String grit_id = "";
                String action = "CREATE";
                String type = "NODE";
                String label = node.getLabels().toString().replace("[", "").replace("]", "");
                Map<String, Object> properties = node.getAllProperties();

                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                    properties.put("grit_id", grit_id);
                }

                String propertiesStr = properties.toString();




                //Map<String, String> content = new HashMap<>();
                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("label", label);
                content.put("properties", propertiesStr);
                content.put("username", username);

                contents.append(toJson(content));
            }

            //Created Relationship
            for (Relationship relationship : td.createdRelationships()) {
                HashMap<String, String> content = new HashMap();
                String id = String.valueOf(relationship.getId());
                String grit_id = "";
                String grit_start_node_id = "";
                String grit_end_node_id = "";
                String action = "CREATE";
                String type = "EDGE";
                String relationshipType = relationship.getType().toString().replace("[", "").replace("]", "");

                String startNodeId = String.valueOf(relationship.getStartNode().getId());
                String endNodeId = String.valueOf(relationship.getEndNode().getId());
                String startNodeLabel = relationship.getStartNode().getLabels().toString().replace("[", "").replace("]", "");
                String endNodeLabel = relationship.getEndNode().getLabels().toString().replace("[", "").replace("]", "");

                Map<String, Object> properties = relationship.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                    properties.put("grit_id", grit_id);
                }

                if(properties.containsKey("grit_start_node_id")){
                    grit_start_node_id = properties.get("grit_start_node_id").toString();
                } else {
                    grit_start_node_id = startNodeId;
                    properties.put("grit_start_node_id", grit_start_node_id);
                }

                if(properties.containsKey("grit_end_node_id")){
                    grit_end_node_id = properties.get("grit_end_node_id").toString();
                } else {
                    grit_end_node_id = endNodeId;
                    properties.put("grit_end_node_id", grit_end_node_id);
                }

                String propertiesStr = properties.toString();

                //Map<String, String> content = new HashMap<>();
                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("relationship_type", relationshipType);
                content.put("start_node_id", startNodeId);
                content.put("end_node_id", endNodeId);
                content.put("start_node_label", startNodeLabel);
                content.put("end_node_label", endNodeLabel);
                content.put("properties", propertiesStr);
                content.put("username", username);

                contents.append(toJson(content));
            }

            //Assigned node properties
            for(PropertyEntry<Node> nodePropertyEntry : td.assignedNodeProperties()){
                HashMap<String, String> content = new HashMap();
                Node node = nodePropertyEntry.entity();
                String action = "UPDATE";
                String type = "NODE";
                String id = String.valueOf(node.getId());
                String grit_id = "";
                Map<String, Object> properties = node.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                }

                String key = nodePropertyEntry.key();

                String newValue = "";
                if(nodePropertyEntry.value() != null){
                    newValue = nodePropertyEntry.value().toString();
                }

                String oldValue = "";
                if(nodePropertyEntry.previouslyCommitedValue() != null) {
                    oldValue = nodePropertyEntry.previouslyCommitedValue().toString();
                }

                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("key", key);
                content.put("new_value", newValue);
                content.put("old_value", oldValue);
                content.put("username", username);

                contents.append(toJson(content));
            }

            //Assigned relationship properties
            for(PropertyEntry<Relationship> relationshipPropertyEntry : td.assignedRelationshipProperties()){
                HashMap<String, String> content = new HashMap();
                Relationship relationship = relationshipPropertyEntry.entity();
                String action = "UPDATE";
                String type = "EDGE";
                String id = String.valueOf(relationship.getId());
                String grit_id = "";
                Map<String, Object> properties = relationship.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                }

                String key = relationshipPropertyEntry.key();

                String newValue = "";
                if(relationshipPropertyEntry.value() != null){
                    newValue = relationshipPropertyEntry.value().toString();
                }

                String oldValue = "";
                if(relationshipPropertyEntry.previouslyCommitedValue() != null) {
                    oldValue = relationshipPropertyEntry.previouslyCommitedValue().toString();
                }

                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("key", key);
                content.put("new_value", newValue);
                content.put("old_value", oldValue);
                content.put("username", username);

                contents.append(toJson(content));
            }

            //////////

            //Deleted Node
            for (Node node : td.deletedNodes()) {
                /*
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {
                    bw.write("CU, delete node \r\n");
                    bw.write(node.toString() + "\r\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
                HashMap<String, String> content = new HashMap();
                String id = String.valueOf(node.getId());
                String grit_id = "";
                String action = "DELETE";
                String type = "NODE";
                String label = node.getLabels().toString().replace("[", "").replace("]", "");
                Map<String, Object> properties = node.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                    properties.put("grit_id", grit_id);
                }
                String propertiesStr = properties.toString();


                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("label", label);
                content.put("properties", propertiesStr);

                contents.append(toJson(content));

            }

            //Deleted Relationship
            for (Relationship relationship : td.deletedRelationships()) {
                /*
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {

                    bw.write("CU, delete rel \r\n");
                    bw.write(relationship.toString() + "\r\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
                HashMap<String, String> content = new HashMap();
                String id = String.valueOf(relationship.getId());
                String grit_id = "";
                String grit_start_node_id = "";
                String grit_end_node_id = "";
                String action = "DELETE";
                String type = "EDGE";
                String relationshipType = relationship.getType().name();

                String startNodeId = String.valueOf(relationship.getStartNode().getId());
                String endNodeId = String.valueOf(relationship.getEndNode().getId());
                String startNodeLabel = relationship.getStartNode().getLabels().toString().replace("[", "").replace("]", "");
                String endNodeLabel = relationship.getEndNode().getLabels().toString().replace("[", "").replace("]", "");

                Map<String, Object> properties = relationship.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                    properties.put("grit_id", grit_id);
                }

                if(properties.containsKey("grit_start_node_id")){
                    grit_start_node_id = properties.get("grit_start_node_id").toString();
                } else {
                    grit_start_node_id = startNodeId;
                    properties.put("grit_start_node_id", grit_start_node_id);
                }

                if(properties.containsKey("grit_end_node_id")){
                    grit_end_node_id = properties.get("grit_end_node_id").toString();
                } else {
                    grit_end_node_id = endNodeId;
                    properties.put("grit_end_node_id", grit_end_node_id);
                }
                String propertiesStr = properties.toString();

                //Map<String, String> content = new HashMap<>();
                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("relationship_type", relationshipType);
                content.put("start_node_id", startNodeId);
                content.put("end_node_id", endNodeId);
                content.put("start_node_label", startNodeLabel);
                content.put("end_node_label", endNodeLabel);
                content.put("properties", propertiesStr);

                contents.append(toJson(content));
            }

            for(PropertyEntry<Node> nodePropertyEntry : td.removedNodeProperties()){
                /*
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {
                    bw.write("CU, removedNodeProp \r\n");
                    bw.write(nodePropertyEntry.toString() + "\r\n");
                    //work until here
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
                HashMap<String, String> content = new HashMap();
                Node node = nodePropertyEntry.entity();
                String action = "UPDATE";
                String type = "NODE";
                String id = String.valueOf(node.getId());
                String grit_id = "";
                Map<String, Object> properties = node.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                }

                String key = nodePropertyEntry.key();

                String newValue = "";
                if(nodePropertyEntry.value() != null){
                    newValue = nodePropertyEntry.value().toString();
                }

                String oldValue = "";
                if(nodePropertyEntry.previouslyCommitedValue() != null) {
                    oldValue = nodePropertyEntry.previouslyCommitedValue().toString();
                }

                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("key", key);
                content.put("new_value", newValue);
                content.put("old_value", oldValue);

                contents.append(toJson(content));
            }

            for(PropertyEntry<Relationship> relationshipPropertyEntry : td.removedRelationshipProperties()){
                /*
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {

                    bw.write("CU, removedRelProp \r\n");
                    bw.write(relationshipPropertyEntry.toString() + "\r\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
                HashMap<String, String> content = new HashMap();
                Relationship relationship = relationshipPropertyEntry.entity();
                String action = "UPDATE";
                String type = "EDGE";
                String id = String.valueOf(relationship.getId());
                String grit_id = "";
                Map<String, Object> properties = relationship.getAllProperties();
                if(properties.containsKey("grit_id")){
                    grit_id = properties.get("grit_id").toString();
                } else {
                    grit_id = id;
                }

                String key = relationshipPropertyEntry.key();

                String newValue = "";
                if(relationshipPropertyEntry.value() != null){
                    newValue = relationshipPropertyEntry.value().toString();
                }

                String oldValue = "";
                if(relationshipPropertyEntry.previouslyCommitedValue() != null) {
                    oldValue = relationshipPropertyEntry.previouslyCommitedValue().toString();
                }

                content.put("id", id);
                content.put("grit_id", grit_id);
                content.put("action", action);
                content.put("type", type);
                content.put("key", key);
                content.put("new_value", newValue);
                content.put("old_value", oldValue);

                contents.append(toJson(content));
            }

            System.out.println(contents.toString());
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {
                bw.write(contents.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private String toJson(HashMap<String, String> content){

        StringJoiner json = new StringJoiner(",", "{", "}");
        for (Map.Entry<String, String> entry : content.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String entryString = "";
            /**
             * properties:{name=Alpha, grit_id=123}
             * properties=[{"name":"Alpha","grit_id":"123"}]
             */


            if (key.contentEquals("properties")) {
                value = value.replace("{", "[{\"");
                value = value.replace("=", "\":\"");
                value = value.replace(", ", "\",\"");
                value = value.replace("}", "\"}]");
                json.add(String.format("\"%s\":%s", key, value));
                //entryString = String.format("\"%s\":%s,", key, value);
            } else if (key.contentEquals("label") || key.contentEquals("relationship_type")) {
                value = value.replace("[", "");
                value = value.replace("]", "");
                json.add(String.format("\"%s\":\"%s\"", key, value));
            } else {
                json.add(String.format("\"%s\":\"%s\"", key, value));
            }

        }
        String result = json.toString() + "\r\n";
        return result;


    }


}
