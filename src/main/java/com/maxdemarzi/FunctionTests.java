package com.maxdemarzi;

import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.event.LabelEntry;
import org.neo4j.graphdb.event.TransactionData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import com.google.gson.*;

public class FunctionTests {
    private static final String FILENAME = "/Users/ulinnuha/Downloads/neo4j/logtest.txt";

    public static void main(String args[]){
        //System.out.println(null.toString())
    }

    private static void _writeJsonToFile(){

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {
            String action = "CREATE";
            String type = "NODE";
            long id = 10;
            JSONObject content = new JSONObject();
            content.put("id", id);
            content.put("grit_id", id);
            content.put("action", action);
            content.put("type", type);
            System.out.println(content.toString());
            bw.write(content.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void _gsonnn(){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {
            String action = "CREATE";
            String type = "NODE";
            String id = "10";
            Map<String, String> content = new HashMap<>();
            content.put("id", id);
            content.put("grit_id", id);
            content.put("action", action);
            content.put("type", type);
            content.put("label", "[Person]");
            Map<String, String> prop = new HashMap<>();
            //prop.put("name", "Tony Stark");
            //prop.put("ud", "123");
            System.out.println("prop:" + prop.toString() +prop.size());
            content.put("properties", prop.toString());
            String result = toJson(content);
            //String result = new Gson().toJson(content).toString();
            System.out.println(result);
            //Gson gson = new Gson();
            //System.out.println(gson.toJson(content));
            //System.out.println(gson.toJson(content).length());
            //bw.write(gson.toJson(content));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String toJson(Map<String, String> content){
        StringBuilder result = new StringBuilder();
        result.append("{");
        for (Map.Entry<String, String> entry : content.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String entryString = "";
            /**
             * properties:{name=Alpha, grit_id=123}
             * properties=[{"name":"Alpha","grit_id":"123"}]
             */


            if(key.contentEquals("properties")){
                value = value.replace("{", "[{\"");
                value = value.replace("=", "\":\"");
                value = value.replace(", ", "\",\"");
                value = value.replace("}", "\"}]");
                entryString = String.format("\"%s\":%s,", key, value);
            } else if (key.contentEquals("label") || key.contentEquals("relationshipType")){
                value = value.replace("[","");
                value = value.replace("]", "");
                entryString = String.format("\"%s\":\"%s\",", key, value);
            } else {
                entryString = String.format("\"%s\":\"%s\",", key, value);
            }
            result.append(entryString);
        }
        String resultStr = result.toString();
        resultStr = resultStr.substring(0, resultStr.length() - 2);
        resultStr += "}";
        resultStr += "\r\n";
        return resultStr;
    }

    private void _match_node_and_write(){


    }

    private void get_current_user(){

    }
}
