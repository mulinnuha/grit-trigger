package com.maxdemarzi;

import com.github.javafaker.HarryPotter;
import org.neo4j.driver.v1.*;

import static org.neo4j.driver.v1.Values.parameters;
import org.neo4j.graphdb.Node;
import com.github.javafaker.Faker;


public class HelloWorldExample {
    // Driver objects are thread-safe and are typically made available application-wide.
    Driver driver;

    public HelloWorldExample(String uri, String user, String password) {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
    }

    private void addPerson(String name) {
        // Sessions are lightweight and disposable connection wrappers.
        try (Session session = driver.session()) {
            // Wrapping Cypher in an explicit transaction provides atomicity
            // and makes handling errors much easier.
            try (Transaction tx = session.beginTransaction()) {
                tx.run("CREATE (a:Person {name: {x}})", parameters("x", name));
                tx.success();  // Mark this write as successful.
                //printMeNode(name);
            }
        }
    }

    private void addRelationship(String fromPersonName, String toPersonName, String type){

    }

    private void printPeople(String initial) {
        try (Session session = driver.session()) {
            // Auto-commit transactions are a quick and easy way to wrap a read.
            StatementResult result = session.run(
                    "MATCH (a:Person) WHERE a.name STARTS WITH {x} RETURN a.name AS name",
                    parameters("x", initial));
            // Each Cypher execution returns a stream of records.
            while (result.hasNext()) {
                Record record = result.next();

                // Values can be extracted from a record by index or name.
                System.out.println(record.get("name").asString());
            }
        }
    }

    private void matchPeople(String name){

    }

    public void close() {
        // Closing a driver immediately shuts down all open connections.
        driver.close();
    }

    public static void main(String... args) {
        HelloWorldExample example = new HelloWorldExample("bolt://localhost:7687", "neo4j", "qazwsxedc");
        //example.addPerson("Peter");
        //example.addPerson("Parker");
        //example.addPerson("Quill");
        for (int i=0; i<10; i++) {
            Faker faker = new Faker();
            String name = faker.gameOfThrones().character();
            System.out.println(name);
            example.addPerson(name);
        }
        //example.printPeople("T");
        example.close();
    }

    public static void createCommand(HelloWorldExample example){
        example.addPerson("Peter");
        example.addPerson("Parker");
        example.addPerson("Quill");
        //example.printPeople("T");
    }

    private void get_current_user(){

    }


}